from datetime import date

import sqlalchemy
from fastapi import HTTPException
from sqlalchemy import func, select, insert

from src.costs.models import user_expenses


async def get_agr_statistic(session, user):
    query = select(
        func.date_trunc('month', user_expenses.c.date + sqlalchemy.text("interval '1 day'")).label('month'),
        func.sum(user_expenses.c.sum).label('total_sum')
    ).where(user_expenses.c.user_id == user.id).group_by('month').order_by('month')

    result = await session.execute(query)
    records = result.all()

    statistic = [
        {
            'month': record.month.strftime('%Y-%m'),
            'total_sum': record.total_sum
        }
        for record in records
    ]

    return statistic


async def get_all_statistic(session, user):
    query = select(user_expenses.c.description, user_expenses.c.date, user_expenses.c.sum, user_expenses.c.id) \
        .where(user_expenses.c.user_id == user.id)
    result = await session.execute(query)
    records = result.mappings().all()
    return records


async def add_expense_curent_user(new_operation, session, user):
    new_operation_data = new_operation.dict()
    new_operation_data['user_id'] = user.id
    new_operation_data['date'] = new_operation_data['date'] = date.today()
    stmt = insert(user_expenses).values(**new_operation_data)
    await session.execute(stmt)
    await session.commit()
    return {"status": "success"}


async def delete_expense(expense_id, session, user):
    query = user_expenses.delete().where(user_expenses.c.id == expense_id) \
        .where(user_expenses.c.user_id == user.id)
    result = await session.execute(query)
    if result.rowcount == 0:
        raise HTTPException(status_code=404, detail="Expense not found")
    await session.commit()
    return {"status": "success"}

async def search_expenses_by_date(session, user, start_date, end_date):
    query = select(user_expenses.c.description, user_expenses.c.date, user_expenses.c.sum, user_expenses.c.id) \
        .where(
            (user_expenses.c.user_id == user.id) &
            (user_expenses.c.date >= start_date) &
            (user_expenses.c.date <= end_date)
        )

    result = await session.execute(query)
    records = result.mappings().all()
    return records