from sqlalchemy import  MetaData, Table, Column, Integer, String, Date, ForeignKey
from src.auth.models import user


metadata = MetaData()
user_expenses = Table(
    'user_expenses',
    metadata,
    Column('id', Integer, primary_key=True),
    Column('date', Date),
    Column('user_id', Integer, ForeignKey(user.c.id)),
    Column('sum', Integer),
    Column('description', String)
)

