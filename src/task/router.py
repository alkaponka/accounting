from fastapi import APIRouter, BackgroundTasks, Depends, HTTPException

from src.auth.base_config import current_user
from .schema import ReportRequestSchema

from .tasks import send_email_report_self

router = APIRouter(prefix="/report")


@router.post("/self")
def get_dashboard_report(
    report_request: ReportRequestSchema,
    beckground_tasks: BackgroundTasks,
    user=Depends(current_user)
    ):
    try:
        beckground_tasks.add_task(send_email_report_self,
                                  username = user.username,
                                  usermail = report_request.email,
                                  start_date= report_request.start_date,
                                  end_date= report_request.end_date)

        return {
            "status": 200,
            "data": None,
            "details": "The mail has been sent"
        }
    except Exception as e:
        raise HTTPException(status_code=500, detail=
        {
            "status": "500",
            "data": None,
            "details": str(e)
        })

