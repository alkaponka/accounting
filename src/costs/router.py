from fastapi import APIRouter, Depends, HTTPException
from fastapi_cache.decorator import cache
from sqlalchemy.ext.asyncio import AsyncSession

from src.auth.base_config import current_user
from src.auth.models import User
from src.costs.crud import get_agr_statistic, get_all_statistic, add_expense_curent_user, delete_expense, \
    search_expenses_by_date
from src.costs.schemas import ExpenseCreate
from src.database import get_async_session
from src.task.schema import SamplePerPeriod

router = APIRouter(
    prefix="/expenses",
    tags=["expenses"]
)


@router.get("/all")
@cache(expire=60)
async def all_expense(session: AsyncSession = Depends(get_async_session),
                      user: User = Depends(current_user)
                      ):
    try:
        records = await get_all_statistic(session=session, user=user)
        return {
            "status": "200",
            "data": records,
            "details": "All operations are successful "
        }
    except Exception as e:
        raise HTTPException(status_code=500, detail=
        {
            "status": "500",
            "data": None,
            "details": str(e)
        })


@router.get("/agr")
@cache(expire=60)
async def agr_statistic(session: AsyncSession = Depends(get_async_session),
                        user: User = Depends(current_user)
                        ):
    try:
        data = await get_agr_statistic(session=session, user=user)
        return {
            "status": "200",
            "data": data,
            "details": "All operations are successful "
        }
    except Exception as e:
        raise HTTPException(status_code=500, detail=
        {
            "status": "500",
            "data": None,
            "details": str(e)
        })


@router.post("/")
async def add_expense(new_operation: ExpenseCreate,
                      session: AsyncSession = Depends(get_async_session),
                      user: User = Depends(current_user)
                      ):
    try:
        add_data = await add_expense_curent_user(new_operation=new_operation, session=session, user=user)
        return {
            "status": "200",
            "data": add_data,
            "details": "All operations are successful "
        }
    except Exception as e:
        raise HTTPException(status_code=500, detail=
        {
            "status": "500",
            "data": None,
            "details": str(e)
        })


@router.delete("/{expense_id}")
async def del_expense(
        expense_id: int,
        session: AsyncSession = Depends(get_async_session),
        user: User = Depends(current_user)
):
    del_expense_result = await delete_expense(expense_id=expense_id, session=session, user=user)
    return {
        "status": "200",
        "data": None,
        "details": del_expense_result
    }


@router.post("/statistics")
async def get_statistics(date: SamplePerPeriod,
                         session: AsyncSession = Depends(get_async_session),
                         user: User = Depends(current_user)):
    try:
        result = await search_expenses_by_date(session, user, start_date=date.start_date, end_date=date.end_date)
        return {
            "status": "200",
            "data": result,
            "details": "All operations are successful "
        }
    except Exception as e:
        raise HTTPException(status_code=500, detail=
        {
            "status": "500",
            "data": None,
            "details": str(e)
        })
