import smtplib
from email.message import EmailMessage


from src.config import SMTP_PASSWORD, SMTP_USER

SMTP_HOST = "smtp.gmail.com"
SMTP_PORT = 465

def get_email_template_dashboard(username: str, usermail: str, start_date, end_date):
    email = EmailMessage()
    email['Subject'] = 'Statistic expenses for month'
    email['From'] = SMTP_USER
    email['To'] = usermail

    email.set_content(
        '<div>'
        f'<h1 style="color: red;">Hello, {username}, and here is your report at {start_date} --- {end_date}. 😊</h1>'
        '</div>',
        subtype='html'
    )
    return email


def send_email_report_self(username: str, usermail: str, start_date, end_date):
    email = get_email_template_dashboard(username, usermail, start_date, end_date)
    with smtplib.SMTP_SSL(SMTP_HOST, SMTP_PORT) as server:
        server.login(SMTP_USER, SMTP_PASSWORD)
        server.send_message(email)