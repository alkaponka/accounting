from pydantic import BaseModel


class ExpenseCreate(BaseModel):
    sum: int
    description: str