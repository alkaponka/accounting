from fastapi import FastAPI
import uvicorn
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend

from auth.base_config import auth_backend, fastapi_users
from auth.schemas import UserRead, UserCreate
from costs.router import router as router_expenses
from task.router import router as router_tasks
from redis import asyncio as aioredis

app = FastAPI(
    title="Balance App"
)

app.include_router(
    fastapi_users.get_auth_router(auth_backend),
    prefix="/auth",
    tags=["Auth"],
)

app.include_router(
    fastapi_users.get_register_router(UserRead, UserCreate),
    prefix="/auth",
    tags=["Auth"],
)

app.include_router(router_expenses)
app.include_router(router_tasks)


@app.on_event("startup")
async def startup():
    redis = aioredis.from_url(f"redis://localhost")
    FastAPICache.init(RedisBackend(redis), prefix="fastapi-cache")

if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.0", port=8000, reload=True)


