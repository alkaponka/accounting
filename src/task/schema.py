from pydantic import BaseModel
from datetime import date

class SamplePerPeriod(BaseModel):
    start_date: date
    end_date: date

class ReportRequestSchema(SamplePerPeriod):
    email: str